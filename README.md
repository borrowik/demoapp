# Demo Web Application with SpringBoot and React #

Demo application is simple order management website that allows to operate with orders and users.
Example is build with React.js at fronted and SpringBoot at backend.


*initializeTestData* spring profile starts application on local environment with example users and orders data when.

* Users
| User Name   |          Email         |  Password  |
| ----------- | ---------------------- | ----------:|
| borrowik    | myemail@perun.com      |  admin     |
| user123     | myemail123@perun.com   |  user      |

* Orders  
| Order Number   |     Description  |
| -------------- | ----------------:|
| OP1            | Pierogi          |
| OP1            | Kapusta          |