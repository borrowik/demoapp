import React from 'react';
import { Provider } from 'react-redux';
import { Switch , Route , BrowserRouter as Router } from "react-router-dom";
import { Container , Row, Col } from 'react-bootstrap';
import store from './store/';
import NavigationBar from './components/NavigationBar';
import WelcomePage from './components/pages/WelcomePage';
import OrderDashboard from './components/pages/OrderDashboard';
import ContactPage from './components/pages/ContactPage';
import Footer from './components/Footer';
import 'bootstrap/dist/css/bootstrap.min.css';
import './App.css';

const App: React.FC = () => {

  const marginTop = {
    "marginTop":"20px"
  };

  return (
    <Provider store={store}>
        <Router>
                <NavigationBar/>
                <Container style= {marginTop}>
                    <Row>
                        <Col>
                            <Switch>
                                <Route path="/" exact component={WelcomePage}/>
                                <Route path="/orders" exact component={OrderDashboard}/>
                                <Route path="/contact" exact component={ContactPage}/>
                            </Switch>
                        </Col>
                    </Row>
                </Container>
                <Footer/>
        </Router>
    </Provider>
  );
}

export default App;