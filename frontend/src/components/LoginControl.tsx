import React from 'react';

type Props = {
    isLoggedIn : boolean;
};

const LoginControl: React.FC = () => {

    const isLoggedIn = false;

    return  (

          <div className="dropdown-item">
                  {isLoggedIn ? ( 'LogOut' ) :( 'LogIn' )}
          </div>

    );

}

export default LoginControl;