import React from 'react';
import { DropdownButton, Dropdown, Navbar , Nav } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import LoginControl from './LoginControl';

const NavigationBar: React.FC = () => {
    return  (

          <Navbar sticky="top" bg="dark" variant="dark">
            <Link to="" className="navbar-brand"> DemoApp</Link>
            <Nav className="mr-auto">
              <Link to="" className="nav-link">
                <img src="./iconfinder_home-house_2932347.png" height="42" width="42" alt=""/> Welcome</Link>
              <Link to="orders" className="nav-link">
                <img src="./iconfinder_handshake-agree_2932348.png" height="42" width="42" alt=""/> Orders</Link>
              <Link to="contact" className="nav-link">
                <img src="./iconfinder_team-people-group_2932353.png" height="42" width="42" alt=""/> Contact</Link>
             </Nav>
             <DropdownButton
                id="dropdown-menu-button" title=" Menu"
                drop='left'>
               <Dropdown.Item>
                   <Link to="" className="dropdown-item">Welcome</Link>
               </Dropdown.Item>
               <Dropdown.Item>
                   <Link to="orders" className="dropdown-item">Orders</Link>
               </Dropdown.Item>
               <Dropdown.Item >
                   <Link to="contact" className="dropdown-item">Contact</Link>
                </Dropdown.Item>
                <Dropdown.Divider />
                <Dropdown.Item >
                   <LoginControl />
                </Dropdown.Item>
             </DropdownButton>
          </Navbar>

    );

}

export default NavigationBar;