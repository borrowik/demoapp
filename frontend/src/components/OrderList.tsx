import React from 'react';
import { connect } from 'react-redux';
import { OrderResponse } from '../types/OrderResponse';
import { fetchOrders } from '../actions/orderAction';

type Props = {
    isLoading: boolean;
    orders: OrderResponse[];
};

class OrderList extends React.Component<Props, {}> {

  readonly props: Props = {
     orders: [],
     isLoading: true
  };

  async componentDidMount() {
      fetchOrders();
  }

  render() {
    const {orders, isLoading} = this.props;

    if (isLoading) {
      return <p>Loading...</p>;
    }

    if (!orders || orders.length === 0) {
      return <p>No Orders available</p>;
    }

    return (
          <div>
            <h2>List of orders:</h2>
            {orders.map(order =>
                          <div key={order.id}>
                            {order.orderNumber}
                            {order.description}
                          </div>
                        )}
          </div>
    );
  }
}

const mapStateToProps = (stateObject: any) => {
    return ({
        orders: stateObject.orders.items,
        isLoading: stateObject.orders.isLoading
    });
};

export default connect(mapStateToProps, fetchOrders)(OrderList);