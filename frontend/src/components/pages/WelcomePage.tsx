import React from 'react';
import logo from '../../logo.svg';
import { Container, Row, Col, Jumbotron } from 'react-bootstrap';

const WelcomePage: React.FC = () => {
    return  (

        <Container>
            <Row>
                <Col>
                    <Jumbotron className="bg-dark text-white text-center">
                        <img src={logo} className="App-logo" alt="logo" />
                        <h1>Welcome to DemoApp!</h1>
                        <p>This is web application example created by Maciej Borowik.</p>
                        <p>Demo application is simple order management website that allows to operate with orders and users.</p>
                        <p>Example is build with React.js at fronted and SpringBoot at backend.</p>
                    </Jumbotron>
                </Col>
            </Row>
        </Container>

    );
}

export default WelcomePage;