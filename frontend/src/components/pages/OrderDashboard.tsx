import React from 'react';
import OrderList from '../OrderList';

const OrderDashboard: React.FC = () => {
    return (

        <div className="App-header jumbotron bg-dark">
                <OrderList/>
        </div>

    );
}
export default OrderDashboard;