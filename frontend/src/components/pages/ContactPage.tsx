import React from 'react';
import { Container, Row, Col, Jumbotron } from 'react-bootstrap';
import LocationMapCard from '../cards/LocationMapCard';
import ContactUsComponent from '../contactUs/ContactUsComponent';

const ContactPage: React.FC = () => {
    return  (

        <Container>
            <Row>
                <Col xs={8} >
                    <Jumbotron className="bg-dark text-white">
                        <h1>Contact :</h1>
                        <br/>
                        <Row>
                            <Col>
                                <p>Name:</p>
                                <p>City:</p>
                                <p>Country: </p>
                            </Col>
                            <Col xs={10}>
                                <p>Maciej Borowik</p>
                                <p>Camperdown</p>
                                <p>Australia</p>
                            </Col>
                        </Row>
                    </Jumbotron>
                    <ContactUsComponent />
                </Col>
                <Col>
                    <Jumbotron className="bg-dark text-white">
                        <LocationMapCard />
                    </Jumbotron>
                </Col>
            </Row>
        </Container>

    );
}

export default ContactPage;