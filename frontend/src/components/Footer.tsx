import React from 'react';
import { Navbar , Nav} from 'react-bootstrap';
import { Link } from "react-router-dom";
import '../css/footer.css';

const Footer: React.FC = () => {

    let fullYear = new Date().getFullYear();

    return  (

        <div id="footer" className="text-muted">
          <Navbar bg="dark" variant="dark">
            <Nav className="mr-auto">
              <Link to="" className="nav-link">Welcome</Link>
              <Link to="orders" className="nav-link"> Orders</Link>
              <Link to="contact" className="nav-link"> Contact</Link>
            </Nav>
          </Navbar>
          <Navbar bg="dark" variant="dark">
            <div > Powered by Maciej Borowik , { fullYear }</div>
          </Navbar>
        </div>

    );
}

export default Footer;