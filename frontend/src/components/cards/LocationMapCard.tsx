import React from 'react';
import { Badge, Container, Row, Col } from 'react-bootstrap';
import GoogleMapReact from 'google-map-react';

interface IProps {
    lat: number;
    lng: number;
    text: string;
}

const SingleLocation = ({ text , lat, lng } : IProps) => <div>
    <img src="./iconfinder_pin-location-map_2932355.png" height="42" width="42" alt=""/>
    <Badge variant="secondary">
        {text}
    </Badge>
</div>;


const LocationMapCard: React.FC = () => {

    const defaultProps = {
        center: {
          lat: -33.89,
          lng: 151.20
        },
        zoom: 10
      };

    return  (

        <Container>
            <Row>
                <Col>
                    <img src="./iconfinder_pin-location-map_2932355.png" height="42" width="42" alt=""/>
                    Location :
                </Col>
            </Row>
             <Row>
                 <Col style={{height: 200}}>
                    <GoogleMapReact
                      bootstrapURLKeys={{ key: "key" }}
                      defaultCenter={defaultProps.center}
                      defaultZoom={defaultProps.zoom}
                    >
                      <SingleLocation
                        lat={-33.856766}
                        lng={151.215347}
                        text="Sydney Opera"
                      />
                    </GoogleMapReact>
                 </Col>
             </Row>
        </Container>

    );
}

export default LocationMapCard;