import React, { useState } from 'react';
import { Alert, Button, Card, Form } from 'react-bootstrap';

const ContactUsForm: React.FC = () => {

  const [isPending, setPending] = useState(false);
  const [isSuccess, setSuccess] = useState(false);
  const [showResponse, setShowResponse] = useState(false);
  const [message, setMessage] = useState("");
  const [title, setTitle] = useState("");

  async function handleSend (event: any) {
    event.preventDefault();
    const data = new FormData(event.target);
    setPending(true);
    setShowResponse(false);
    const post = {
                    title: data.get('title'),
                    message: data.get('message')
                 };
    try {
        const response = await fetch('/demoapp/notification', {
          method: 'POST',
          headers: {
            'content-type': 'application/json'
          },
          body: JSON.stringify(post)
        });
        if (!response.ok) throw response;
        const json = await response.json()
        messageConfirmed(json);
    } catch(error) {
        setSuccess(false);
        setPending(false);
        setShowResponse(true);
    }
  };

  function timeout(ms: number) {
      return new Promise(resolve => setTimeout(resolve, ms));
  }

  async function messageConfirmed(result: any) {
    setSuccess(true);
    setPending(false);
    setShowResponse(true);
    setTitle("");
    setMessage("");
    await timeout(3000);
    setShowResponse(false);
  }

    return  (
        <Card.Body>
            <Card.Title>Message :</Card.Title>
            <Card.Text>
                <Form onSubmit={handleSend}>
                    <Form.Group>
                        <Form.Label>Title</Form.Label>
                        <Form.Control name="title" placeholder="Greetings message title example" disabled={isPending}/>
                    </Form.Group>
                    <Form.Group>
                        <Form.Label>Content</Form.Label>
                        <Form.Control name="message" as="textarea" value={message} onChange={(event: React.FormEvent<HTMLSelectElement>) => setMessage(event.currentTarget.value)} rows="4" disabled={isPending}/>
                    </Form.Group>
                    <Button variant="primary" type="submit" disabled={isPending}>
                        {!isPending ? 'Send' : '...Sending'}
                    </Button>
                    <Alert show={showResponse} variant={isSuccess ? 'success' : 'danger'} onClose={() => setShowResponse(false)} dismissible>
                        {isSuccess ? 'Message has been delivered!' : 'Oops.. something went wrong. Try again later.'}
                    </Alert>
                </Form>
            </Card.Text>
        </Card.Body >
    );
}

export default ContactUsForm;