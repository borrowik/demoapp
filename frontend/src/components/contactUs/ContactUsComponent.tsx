import React from 'react';
import { Accordion, Card } from 'react-bootstrap';
import ContactUsForm from './ContactUsForm';

const ContactUsComponent: React.FC = () => {

  const marginBottom = {
    "marginBottom":"20px"
  };

    return  (
        <Accordion style= { marginBottom }>
            <Card >
                <Card.Header className="bg-dark text-light text-center">
                  <Accordion.Toggle as={Card.Header} variant="link" eventKey="1">
                    Write to us!
                  </Accordion.Toggle>
                </Card.Header>
                <Accordion.Collapse eventKey="1">
                    <ContactUsForm />
                </Accordion.Collapse>
            </Card>
        </Accordion >
    );
}

export default ContactUsComponent;