import { FETCH_ORDERS_REQUEST, FETCH_ORDERS_SUCCESS, FETCH_ORDERS_FAILED, NEW_ORDER } from './types';


export function fetchOrders() {
    return function(dispatch: any) {
    dispatch({
                type: FETCH_ORDERS_REQUEST
             });
    fetch('/demoapp/orders')
              .then(response => response.json())
              .then( orders =>
                    dispatch({
                                type: FETCH_ORDERS_SUCCESS,
                                payload: orders
                            }))
    .catch(err =>
        dispatch({
                    type: FETCH_ORDERS_FAILED,
                    payload: err
                }));
    }
}