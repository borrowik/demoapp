export interface OrderResponse {
    id: number;
    orderNumber?: string;
    description?: string;
}