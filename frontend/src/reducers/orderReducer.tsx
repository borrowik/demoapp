import { FETCH_ORDERS_REQUEST, FETCH_ORDERS_SUCCESS, FETCH_ORDERS_FAILED } from '../actions/types';

const initialState = {
    items: [],
    item: {},
    isLoading: false
};

const rootReducer = (state = initialState, action: any) => {
  console.log('reducer', action);
  switch (action.type) {
    case FETCH_ORDERS_REQUEST:
      return {  ...state,
                  items: [],
                  isLoading: true}
    case FETCH_ORDERS_SUCCESS:
      return {  ...state,
                items: action.payload,
                isLoading: false}
    case FETCH_ORDERS_FAILED:
      return {  ...state,
                  items: [],
                  isLoading: false}
    default:
      return state
  }
}

export default rootReducer;