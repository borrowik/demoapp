package com.borowik.demo.initializer;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Profile;
import org.springframework.core.io.ClassPathResource;
import org.springframework.jdbc.datasource.init.DatabasePopulatorUtils;
import org.springframework.jdbc.datasource.init.ResourceDatabasePopulator;
import org.springframework.stereotype.Component;

import javax.sql.DataSource;

@Slf4j
@Component
@Profile("initializeTestData")
public class InitialDataRunner implements CommandLineRunner {

    @Value("${info.environment}")
    private String env;

    @Autowired
    private DataSource dataSource;

    @Override
    public void run(String... args) {
        log.info("Initializing test data");
        if(!"LOCAL".equals(env)) {
            log.warn("Initialization should be used only for local test purposes");
            return;
        }
        ResourceDatabasePopulator populator = new ResourceDatabasePopulator();
        populator.setScripts(new ClassPathResource("db/changelog/demoapp/local/initializeTestData.sql"));
        DatabasePopulatorUtils.execute(populator, dataSource);
    }

}
