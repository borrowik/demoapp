package com.borowik.demo.notification;

import org.springframework.data.repository.RepositoryDefinition;
import org.springframework.stereotype.Repository;

import java.util.Optional;
import java.util.stream.Stream;

@Repository
@RepositoryDefinition(domainClass = Notification.class, idClass = Long.class)
public interface NotificationRepository {

    Stream<Notification> findAll();

    Optional<Notification> findById(Long id);

    Stream<Notification> findByType(Notification.Type notificationType);

    Notification save(Notification order);

    void deleteById(Long id);

}
