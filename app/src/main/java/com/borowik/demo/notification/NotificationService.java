package com.borowik.demo.notification;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

@Service
@RequiredArgsConstructor
public class NotificationService {

    private final NotificationRepository notificationRepository;

    public Notification addNotification(Notification notification) {
        Assert.notNull(notification, "Notification is required");
        return notificationRepository.save(notification);
    }

}

