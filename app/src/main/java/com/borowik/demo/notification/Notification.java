package com.borowik.demo.notification;

import com.borowik.demo.common.AbstractEntity;
import com.borowik.demo.order.OrderNumber;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Getter
@Setter
@NoArgsConstructor // JPA
@Entity(name = "NOTIFICATIONS")
public class Notification extends AbstractEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "NOTIFICATION_ID_SEQ")
    @SequenceGenerator(name = "NOTIFICATION_ID_SEQ", sequenceName = "NOTIFICATION_ID_SEQ")
    Long id;

    @Enumerated(EnumType.STRING)
    private Type type;

    private String title;

    private String body;

    public Notification(Type type, String title , String body) {
        this.type = type;
        this.title = title;
        this.body = body;
    }

    public enum Type {
        CONTACT_MESSAGE, DEFAULT
    }

}
