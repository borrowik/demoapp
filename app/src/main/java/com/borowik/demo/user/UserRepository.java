package com.borowik.demo.user;

import org.springframework.data.repository.RepositoryDefinition;
import org.springframework.stereotype.Repository;

import java.util.Optional;
import java.util.stream.Stream;

@Repository
@RepositoryDefinition(domainClass = User.class, idClass = Long.class)
public interface UserRepository {

    Stream<User> findAll();

    Optional<User> findById(Long id);

    Optional<User> findByEmail(String email);

    User save(User order);

    void deleteById(Long id);

}
