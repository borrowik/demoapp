package com.borowik.demo.user;

import com.borowik.demo.common.AbstractEntity;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.Collection;

import static java.util.Objects.requireNonNull;

@Getter
@Setter
@NoArgsConstructor // JPA
@Entity(name = "PRIVILEGES")
public class Privilege extends AbstractEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "PRIVILEGE_ID_SEQ")
    @SequenceGenerator(name = "PRIVILEGE_ID_SEQ", sequenceName = "PRIVILEGE_ID_SEQ")
    private Long id;

    private String name;

    @ManyToMany(mappedBy = "privileges")
    private Collection<Role> roles;

    Privilege(String name) {
        requireNonNull(name, "name must be present");
        this.name = name;
    }
}
