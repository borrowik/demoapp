package com.borowik.demo.user;

import com.borowik.demo.common.AbstractEntity;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.List;

import static java.util.Objects.requireNonNull;

@Getter
@Setter
@NoArgsConstructor // JPA
@Entity(name = "USERS")
public class User extends AbstractEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "USER_ID_SEQ")
    @SequenceGenerator(name = "USER_ID_SEQ", sequenceName = "USER_ID_SEQ")
    Long id;

    private String userName;

    private String firstName;

    private String lastName;

    private String email;

    private String password;

    @OneToMany
    @JoinTable(
            name = "users_roles",
            joinColumns = @JoinColumn(
                    name = "user_id", referencedColumnName = "id"),
            inverseJoinColumns = @JoinColumn(
                    name = "role_id", referencedColumnName = "id"))
    private List<Role> roles;

    User(String userName, String firstName, String lastName, String email, String password) {
        requireNonNull(userName, "userName must be present");
        requireNonNull(email, "email must be present");
        this.userName = userName;
        this.email = email;
        this.firstName = firstName;
        this.lastName = lastName;
        this.password = password;
    }
}
