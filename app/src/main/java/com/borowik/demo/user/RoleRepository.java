package com.borowik.demo.user;

import org.springframework.data.repository.RepositoryDefinition;
import org.springframework.stereotype.Repository;

import java.util.Optional;
import java.util.stream.Stream;

@Repository
@RepositoryDefinition(domainClass = Role.class, idClass = Long.class)
public interface RoleRepository {

    Stream<Role> findAll();

    Optional<Role> findById(Long id);

    Optional<Role> findByName(String email);

    Role save(Role order);

    void deleteById(Long id);
}
