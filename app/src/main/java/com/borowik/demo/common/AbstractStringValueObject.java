package com.borowik.demo.common;

import lombok.Getter;
import org.springframework.util.Assert;

import java.util.Objects;

@Getter
public class AbstractStringValueObject {

    private final String value;

    public AbstractStringValueObject(String value) {
        Assert.hasLength(value, "Value must not be empty");
        this.value = value;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        AbstractStringValueObject other = (AbstractStringValueObject) obj;
        return Objects.equals(this.value, other.value);
    }

}
