package com.borowik.demo.common;

public final class ObjectUtils {

    public static boolean isBlank(String value) {
        return value == null || value.isBlank();
    }

}
