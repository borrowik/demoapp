package com.borowik.demo.common;

import java.lang.reflect.Constructor;
import java.lang.reflect.ParameterizedType;

import javax.persistence.AttributeConverter;

import org.springframework.util.StringUtils;

public abstract class AbstractStringValueObjectConverter<T extends AbstractStringValueObject> implements AttributeConverter<T, String> {

    private final Constructor<T> constructorOfT;

    @SuppressWarnings("unchecked")
    protected AbstractStringValueObjectConverter() {
        try {
            Class<T> classOfT = (Class<T>) ((ParameterizedType) getClass().getGenericSuperclass()).getActualTypeArguments()[0];
            constructorOfT = classOfT.getConstructor(String.class);
        } catch (RuntimeException ex) {
            throw ex;
        } catch (Exception ex) {
            throw new Error(ex);
        }
    }

    @Override
    public String convertToDatabaseColumn(T stringValueObject) {
        return stringValueObject == null ? null : stringValueObject.getValue();
    }

    @Override
    public T convertToEntityAttribute(String value) {
        try {
            return StringUtils.hasLength(value) ? constructorOfT.newInstance(value) : null;
        } catch (RuntimeException ex) {
            throw ex;
        } catch (Exception ex) {
            throw new Error(ex);
        }
    }

}
