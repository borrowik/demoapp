package com.borowik.demo.order;

import org.springframework.data.repository.RepositoryDefinition;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;
import java.util.stream.Stream;

@Repository
@RepositoryDefinition(domainClass = Order.class, idClass = Long.class)
public interface OrderRepository {

    Stream<Order> findAll();

    Optional<Order> findById(Long id);

    Stream<Order> findByOrderNumber(OrderNumber orderNumber);

    Order save(Order order);

    void deleteById(Long id);

}
