package com.borowik.demo.order;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static java.util.stream.Collectors.toList;

@Service
@RequiredArgsConstructor
public class OrderService {

    private final OrderRepository orderRepository;

    public List<Order> findAll() {
        return orderRepository.findAll().collect(toList());
    }

    public List<Order> findAll(OrderNumber orderNumber) {
        Assert.notNull(orderNumber, "Order number is required");
        return orderRepository.findByOrderNumber(orderNumber).collect(Collectors.toList());
    }

    public Optional<Order> findById(Long orderId) {
        Assert.notNull(orderId, "OrderId is required");
        return orderRepository.findById(orderId);
    }

    public Order saveOrder(Order order) {
        Assert.notNull(order, "Order is required");
        return orderRepository.save(order);
    }

    public void deleteById(Long orderId) {
        Assert.notNull(orderId, "OrderId is required");
        orderRepository.deleteById(orderId);
    }

}

