package com.borowik.demo.order;

import com.borowik.demo.common.AbstractStringValueObject;
import com.borowik.demo.common.AbstractStringValueObjectConverter;

import javax.persistence.Converter;

public final class OrderNumber extends AbstractStringValueObject {

    public OrderNumber(String value) {
        super(value);
    }

    @Converter(autoApply = true)
    public static class OrderNumberConverter extends AbstractStringValueObjectConverter<OrderNumber> {}

}
