package com.borowik.demo.order;

import com.borowik.demo.common.AbstractEntity;
import lombok.*;

import javax.persistence.*;

@Getter
@Setter
@NoArgsConstructor // JPA
@Entity(name = "ORDERS")
public class Order extends AbstractEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "ORDER_ID_SEQ")
    @SequenceGenerator(name = "ORDER_ID_SEQ", sequenceName = "ORDER_ID_SEQ")
    Long id;

    private OrderNumber orderNumber;

    private String description;

    public Order(OrderNumber orderNumber, String description) {
        this.orderNumber = orderNumber;
        this.description = description;
    }

}
