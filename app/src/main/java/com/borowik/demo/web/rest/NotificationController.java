package com.borowik.demo.web.rest;

import com.borowik.demo.notification.Notification;
import com.borowik.demo.notification.NotificationService;
import com.borowik.demo.web.api.ContactMessage;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import static com.borowik.demo.common.ObjectUtils.isBlank;

@RestController
@RequestMapping(value = "/notification", produces = { "application/json" })
@RequiredArgsConstructor
public class NotificationController {

    private final NotificationService notificationService;

    @PostMapping()
    public ResponseEntity<String> create(@RequestBody ContactMessage contactMessage) {
        if (isBlank(contactMessage.getTitle()) || isBlank(contactMessage.getMessage())) {
            return ResponseEntity.badRequest().build();
        }
        Notification notification = new Notification(
                Notification.Type.CONTACT_MESSAGE,
                contactMessage.getTitle(),
                contactMessage.getMessage());
        notification = notificationService.addNotification(notification);
            return ResponseEntity.ok(notification.getId().toString());
    }

}
