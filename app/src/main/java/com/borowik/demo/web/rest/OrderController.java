package com.borowik.demo.web.rest;

import com.borowik.demo.order.Order;
import com.borowik.demo.order.OrderNumber;
import com.borowik.demo.order.OrderService;
import com.borowik.demo.web.api.CreateOrder;
import com.borowik.demo.web.api.OrderResponse;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

import static com.borowik.demo.common.ObjectUtils.isBlank;
import static java.util.stream.Collectors.toList;

@RestController
@RequestMapping(value = "/orders", produces = { "application/json" })
@RequiredArgsConstructor
public class OrderController {

    private final OrderService orderService;
    private final OrderMapper orderMapper;

    @GetMapping()
    public ResponseEntity<List<OrderResponse>> getAll() {
        List<Order> orders = orderService.findAll();
        return ResponseEntity.ok(
                orders.stream()
                        .map(orderMapper::map)
                        .collect(toList())
        );
    }

    @GetMapping("/{id}")
    public ResponseEntity<OrderResponse> getOrder(@PathVariable("id") Long orderId) {
        Optional<Order> order = orderService.findById(orderId);
        return order.map(orderMapper::map)
                .map(ResponseEntity::ok)
                .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    @GetMapping("/search/{orderNumber}")
    public ResponseEntity<List<OrderResponse>> getOrders(@PathVariable("orderNumber") String orderNumber) {
        if (isBlank(orderNumber)) {
            return ResponseEntity.badRequest().build();
        }

        List<Order> orders = orderService.findAll(new OrderNumber(orderNumber));
        return ResponseEntity.ok(
                orders.stream()
                        .map(orderMapper::map)
                        .collect(toList())
        );
    }

    @PostMapping()
    public ResponseEntity<OrderResponse> create(@RequestBody CreateOrder createOrder) {
        if (isBlank(createOrder.getOrderNumber())) {
            return ResponseEntity.badRequest().build();
        }
        Order newOrder = orderMapper.map(createOrder);
        newOrder = orderService.saveOrder(newOrder);
        return ResponseEntity.ok(orderMapper.map(newOrder));
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Long> deleteOrder(@PathVariable("id") Long orderId) {
        if (orderService.findById(orderId).isEmpty()) {
            return ResponseEntity.notFound().build();
        }
        orderService.deleteById(orderId);
        return ResponseEntity.ok(orderId);
    }

}
