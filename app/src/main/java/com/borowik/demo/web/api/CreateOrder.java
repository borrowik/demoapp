package com.borowik.demo.web.api;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;

@Getter
public class CreateOrder {

    private final String orderNumber;

    private final String description;

    @JsonCreator
    public CreateOrder(
            @JsonProperty(value = "orderNumber") String orderNumber,
            @JsonProperty(value = "description") String description) {
        this.orderNumber = orderNumber;
        this.description = description;
    }
}
