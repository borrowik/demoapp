package com.borowik.demo.web.api;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;

@Getter
public class ContactMessage {

    private final String title;

    private final String message;

    @JsonCreator
    public ContactMessage(
            @JsonProperty(value = "title") String title,
            @JsonProperty(value = "message") String message) {
        this.title = title;
        this.message = message;
    }
}
