package com.borowik.demo.web.rest;

import com.borowik.demo.common.AbstractStringValueObject;
import com.borowik.demo.order.Order;
import com.borowik.demo.order.OrderNumber;
import com.borowik.demo.web.api.CreateOrder;
import com.borowik.demo.web.api.OrderResponse;
import org.springframework.stereotype.Component;

@Component
class OrderMapper {

    static String getStringValue(AbstractStringValueObject stringValueObject) {
        return stringValueObject != null ? stringValueObject.getValue() : null;
    }

    OrderResponse map(Order order) {
        return new OrderResponse(order.getId(), getStringValue(order.getOrderNumber()), order.getDescription());
    }

    Order map(CreateOrder createOrder) {
        return new Order(new OrderNumber(createOrder.getOrderNumber()), createOrder.getDescription());
    }

}
