package com.borowik.demo.web.api;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;

@Getter
public class OrderResponse {

    private final Long id;

    private final String orderNumber;

    private final String description;

    @JsonCreator
    public OrderResponse(
            @JsonProperty(value = "id") Long id,
            @JsonProperty(value = "orderNumber") String orderNumber,
            @JsonProperty(value = "description") String description) {
        this.id = id;
        this.orderNumber = orderNumber;
        this.description = description;
    }
}
