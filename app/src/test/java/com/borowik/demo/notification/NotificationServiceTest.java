package com.borowik.demo.notification;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;

class NotificationServiceTest {

    @Mock
    NotificationRepository notificationRepository;

    @InjectMocks
    private NotificationService notificationService;

    @BeforeEach
    void init() {
        initMocks(this);
    }

    @Test
    void shouldFailWhenNoOrderNumber() {
        // when
        Exception exception = assertThrows(
                IllegalArgumentException.class,
                () -> notificationService.addNotification((Notification) null));


        // then
        assertTrue(exception.getMessage().contains("Notification is required"));
    }

    @Test
    void shouldSaveOrder() {
        // given
        Notification notification = new Notification(Notification.Type.CONTACT_MESSAGE,"A", "B");
        when(notificationRepository.save(notification)).thenReturn(notification);

        // when
        Notification result = notificationService.addNotification(notification);

        // then
        verify(notificationRepository).save(notification);
        assertThat(result, is(notification));
    }
}