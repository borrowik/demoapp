package com.borowik.demo.notification;

import com.borowik.demo.common.AbstractRepositoryIntegrationTest;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

import static java.util.stream.Collectors.toList;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.Matchers.hasSize;
import static org.junit.Assert.assertThat;

class NotificationRepositoryIntegrationTest extends AbstractRepositoryIntegrationTest {

    @Autowired
    private NotificationRepository notificationRepository;

    @BeforeEach
    void setup() {
        notificationRepository.findAll().map(Notification::getId).forEach(notificationRepository::deleteById);
    }

    @Test
    void shouldContainNoEntries() {
        // when + then
        assertThat(notificationRepository.findAll().count(), is(0L));
    }

    @Test
    void shouldSaveSingleNotification() {
        // given
        String title = "Notification title";
        String body = "Notification body details";
        Notification newNotification = new Notification(Notification.Type.CONTACT_MESSAGE, title, body);

        // when
        notificationRepository.save(newNotification);
        List<Notification> savedNotifications = notificationRepository.findByType(Notification.Type.CONTACT_MESSAGE).collect(toList());

        // then
        assertThat(savedNotifications, hasSize(1));
        Notification savedNotification = savedNotifications.get(0);
        assertThat(savedNotification.getId(), is(newNotification.getId()));
        assertThat(savedNotification.getType(), is(Notification.Type.CONTACT_MESSAGE));
        assertThat(savedNotification.getTitle(), is(newNotification.getTitle()));
        assertThat(savedNotification.getBody(), is(newNotification.getBody()));
    }

}