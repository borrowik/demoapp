package com.borowik.demo.common;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ObjectUtilsTest {

    @Test
    void shouldTrueOnNullValue() {
        // when
        boolean result = ObjectUtils.isBlank(null);

        // then
        assertTrue(result);
    }

    @Test
    void shouldTrueOnBlank() {
        // when
        boolean result = ObjectUtils.isBlank(" ");

        // then
        assertTrue(result);
    }

    @Test
    void shouldFalseOnNonBlank() {
        // when
        boolean result = ObjectUtils.isBlank(" s ");

        // then
        assertFalse(result);
    }
}