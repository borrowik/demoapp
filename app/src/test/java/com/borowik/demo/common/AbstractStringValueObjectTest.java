package com.borowik.demo.common;

import com.borowik.demo.order.OrderNumber;
import org.junit.jupiter.api.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

class AbstractStringValueObjectTest {

    @Test
    void shouldRejectNullValue() {
        // when
        Exception exception = assertThrows(
                IllegalArgumentException.class,
                () -> new OrderNumber(null));


        // then
        assertTrue(exception.getMessage().contains("Value must not be empty"));
    }

    @Test
    void shouldRejectEmptyValue() {
        // when
        Exception exception = assertThrows(
                IllegalArgumentException.class,
                () -> new OrderNumber(""));


        // then
        assertTrue(exception.getMessage().contains("Value must not be empty"));
    }

    @Test
    void shouldCreateObjectValue() {
        // when
        AbstractStringValueObject orderNumber = new OrderNumber("Value");

        // then
        assertThat(orderNumber.getValue(), is("Value"));
    }

}