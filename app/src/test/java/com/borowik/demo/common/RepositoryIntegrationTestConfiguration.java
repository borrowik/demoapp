package com.borowik.demo.common;

import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@Configuration
@EnableJpaRepositories(basePackages="com.borowik.demo")
@EnableTransactionManagement
class RepositoryIntegrationTestConfiguration {
}
