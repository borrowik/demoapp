package com.borowik.demo.common;

import com.borowik.demo.DemoApplication;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.context.WebApplicationContext;

import java.io.IOException;
import java.util.List;

@ExtendWith(SpringExtension.class)
@SpringBootTest(
        classes = { DemoApplication.class, RepositoryIntegrationTestConfiguration.class },
        webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ActiveProfiles("test")
@Transactional
public abstract class AbstractApplicationIntegrationTest {

    @Autowired
    private WebApplicationContext context;

    protected MockMvc mvc;

    private ObjectMapper objectMapper;

    @BeforeEach
    public void setup() {
        mvc = MockMvcBuilders
                .webAppContextSetup(context)
                .build();
        objectMapper = new ObjectMapper();
        objectMapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);
    }

    protected  <T> T mapToObject(MockHttpServletResponse source, Class<T> clazz) throws IOException {
        return objectMapper.readValue(source.getContentAsString(), clazz);
    }

    protected <T> List<T> mapToObjectList(MockHttpServletResponse source, Class<T> clazz) throws IOException {
        return objectMapper.readValue(source.getContentAsString(),
                objectMapper.getTypeFactory().constructCollectionType(List.class, clazz));
    }

}

