package com.borowik.demo.web;

import com.borowik.demo.common.AbstractApplicationIntegrationTest;
import com.borowik.demo.order.Order;
import com.borowik.demo.order.OrderNumber;
import com.borowik.demo.order.OrderRepository;
import com.borowik.demo.web.api.OrderResponse;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;
import static org.hamcrest.Matchers.notNullValue;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

class OrderControllerIntegrationTest extends AbstractApplicationIntegrationTest {

    @Autowired
    private OrderRepository orderRepository;

    @Test
    void shouldGetOrder() throws Exception {
        // given
        Order order = new Order(new OrderNumber("ON1"), "Order description 1");
        orderRepository.save(order);
        Long orderId = order.getId();

        // when
        MvcResult result = mvc.perform(MockMvcRequestBuilders
                .get("/orders/{id}", orderId)
                .accept(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk())
                .andReturn();

        // then
        OrderResponse orderResponse = mapToObject(result.getResponse(), OrderResponse.class);
        assertThat(orderResponse, is(notNullValue()));
        assertThat(orderResponse.getId(), is(order.getId()));
        assertThat(orderResponse.getOrderNumber(), is(order.getOrderNumber().getValue()));
        assertThat(orderResponse.getDescription(), is(order.getDescription()));
    }

    @Test
    void shouldDeleteOrder() throws Exception {
        // given
        Order order = new Order(new OrderNumber("ON1"), "Order description 1");
        orderRepository.save(order);
        Long orderId = order.getId();

        // when
        MvcResult result = mvc.perform(MockMvcRequestBuilders
                .delete("/orders/{id}", orderId)
                .accept(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk())
                .andReturn();

        // then
        Long deletedId = mapToObject(result.getResponse(), Long.class);
        assertThat(deletedId, is(orderId));
    }

    @Test
    @Sql(scripts = "/db/changelog/demoapp/local/initializeTestData.sql")
    void shouldInertInitialTestData() throws Exception {
        // given
        assertThat(orderRepository.findAll().count(), is(greaterThan(0L)));

        // when
        MvcResult result = mvc.perform(MockMvcRequestBuilders
                .get("/orders")
                .accept(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk())
                .andReturn();

        // then
        List<OrderResponse> orderResponses = mapToObjectList(result.getResponse(), OrderResponse.class);
        assertThat(orderResponses, contains(
                allOf(hasProperty("orderNumber", is("OP1")),
                        hasProperty("description", is("Pierogi"))),
                allOf(hasProperty("orderNumber", is("OP2")),
                        hasProperty("description", is("Kapusta")))));
    }

}
