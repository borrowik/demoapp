package com.borowik.demo.web.rest;

import com.borowik.demo.order.Order;
import com.borowik.demo.order.OrderNumber;
import com.borowik.demo.order.OrderService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.Optional;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.is;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

class OrderControllerTest {

    private MockMvc mockMvc;

    @Mock
    private OrderService orderService;

    @BeforeEach
    void setUp() {
        initMocks(this);
        mockMvc = MockMvcBuilders
                .standaloneSetup(
                        new OrderController(orderService, new OrderMapper()))
                .build();
    }

    @Test
    void shouldFindOrder() throws Exception {
        // given
        long id = 1L;
        OrderNumber orderNumber = new OrderNumber("ON1");
        String description = "Order description 1";
        Order order = new Order(orderNumber, description);
        order.setId(id);
        when(orderService.findById(id)).thenReturn(Optional.of(order));

        // when
        String responseBody = mockMvc.perform(MockMvcRequestBuilders
                .get("/orders/{id}", id)
                .accept(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        // then
        assertThat(responseBody.isEmpty(), is(false));
        assertThat(responseBody, containsString("{\"id\":1"));
        assertThat(responseBody, containsString("\"orderNumber\":\"ON1\""));
        assertThat(responseBody, containsString("\"description\":\"Order description 1\""));
    }

    @Test
    void shouldDeleteOrder() throws Exception {
        // given
        long id = 1L;
        OrderNumber orderNumber = new OrderNumber("ON1");
        String description = "Order description 1";
        Order order = new Order(orderNumber, description);
        order.setId(id);
        when(orderService.findById(id)).thenReturn(Optional.of(order));

        // when
        String responseBody = mockMvc.perform(MockMvcRequestBuilders
                .delete("/orders/{id}", id)
                .accept(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        // then
        assertThat(responseBody.isEmpty(), is(false));
        verify(orderService).deleteById(id);
    }

}
