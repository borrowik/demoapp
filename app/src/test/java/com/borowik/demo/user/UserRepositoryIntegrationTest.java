package com.borowik.demo.user;

import com.borowik.demo.common.AbstractRepositoryIntegrationTest;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Arrays;
import java.util.List;

import static java.util.stream.Collectors.toList;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

class UserRepositoryIntegrationTest extends AbstractRepositoryIntegrationTest {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private RoleRepository roleRepository;

    @BeforeEach
    void setup() {
        userRepository.findAll().map(User::getId).forEach(userRepository::deleteById);
        roleRepository.findAll().map(Role::getId).forEach(roleRepository::deleteById);
    }

    @Test
    void shouldContainNoEntries() {
        // when + then
        assertThat(userRepository.findAll().count(), is(0L));
    }

    @Test
    void shouldSaveSingleUser() {
        // given
        User newUser = new User("borrowik","Maciej", "Borowik", "email@at.com", "admin");

        // when
        userRepository.save(newUser);
        List<User> savedOrders = userRepository.findAll().collect(toList());

        // then
        assertThat(savedOrders, hasSize(1));
        assertThat(savedOrders, contains(newUser));
    }

    @Test
    void shouldDeleteSingleOrder() {
        // given
        User newUser = new User("borrowik","Maciej", "Borowik", "email@at.com", "admin");
        newUser = userRepository.save(newUser);

        // when
        userRepository.deleteById(newUser.getId());

        // then
        assertThat(userRepository.findById(newUser.getId()).isPresent(), is(false));
    }

    @Test
    void shouldSaveSingleUserWithRolesAndPrivileges() {
        // given
        String userName = "borrowik";
        String email = "email@at.com";
        String password = "admin";
        String roleName = "ROLE_ADMIN";
        String privilegeName = "WRITE_PRIVILEGE";
        User newUser = new User(userName, null, null, email, password);
        Role role = new Role(roleName);
        roleRepository.save(role);
        Privilege privilege = new Privilege(privilegeName);
        role.setPrivileges(Arrays.asList(privilege));
        newUser.setRoles(Arrays.asList(role));

        // when
        userRepository.save(newUser);
        List<User> savedUsers = userRepository.findAll().collect(toList());

        // then
        assertThat(savedUsers, hasSize(1));
        User savedUser = savedUsers.get(0);
        assertThat(savedUser, is(newUser));
        assertThat(savedUser.getRoles(), hasSize(1));
        Role savedRole = savedUser.getRoles().get(0);
        assertThat(savedRole, is(role));
        assertThat(savedRole.getPrivileges(), hasSize(1));
        Privilege savedPrivilege = savedRole.getPrivileges().get(0);
        assertThat(savedPrivilege, is(privilege));
    }

}