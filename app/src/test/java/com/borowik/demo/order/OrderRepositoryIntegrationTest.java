package com.borowik.demo.order;

import com.borowik.demo.common.AbstractRepositoryIntegrationTest;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

import static java.util.stream.Collectors.toList;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

class OrderRepositoryIntegrationTest extends AbstractRepositoryIntegrationTest {

    @Autowired
    private OrderRepository orderRepository;

    @BeforeEach
    void setup() {
        orderRepository.findAll().map(Order::getId).forEach(orderRepository::deleteById);
    }

    @Test
    void shouldContainNoEntries() {
        // when + then
        assertThat(orderRepository.findAll().count(), is(0L));
    }

    @Test
    void shouldSaveSingleOrder() {
        // given
        OrderNumber orderNumber = new OrderNumber("ON1");
        String description = "Order description";
        Order newOrder = new Order(orderNumber, description);

        // when
        orderRepository.save(newOrder);
        List<Order> savedOrders = orderRepository.findByOrderNumber(orderNumber).collect(toList());

        // then
        assertThat(savedOrders, hasSize(1));
        assertThat(savedOrders, contains(newOrder));
    }

    @Test
    void shouldDeleteSingleOrder() {
        // given
        OrderNumber orderNumber = new OrderNumber("ON1");
        String description = "Order description";
        Order newOrder = new Order(orderNumber, description);
        orderRepository.save(newOrder);

        // when
        orderRepository.deleteById(newOrder.getId());


        // then
        assertThat(orderRepository.findById(newOrder.getId()).isPresent(), is(false));
    }
}