package com.borowik.demo.order;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;

import java.util.List;
import java.util.Optional;
import java.util.stream.Stream;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;

class OrderServiceTest {

    @Mock
    OrderRepository orderRepository;

    @InjectMocks
    private OrderService orderService;

    @BeforeEach
    void init() {
        initMocks(this);
    }

    @Test
    void shouldFailWhenNoOrderNumber() {
        // when
        Exception exception = assertThrows(
                IllegalArgumentException.class,
                () -> orderService.findAll((OrderNumber) null));


        // then
        assertTrue(exception.getMessage().contains("Order number is required"));
    }

    @Test
    void shouldFindOrderByOrderNumber() {
        // given
        OrderNumber orderNumber = new OrderNumber("numberOne");
        when(orderRepository.findByOrderNumber(orderNumber))
                .thenReturn(Stream.of(new Order(orderNumber, null)));

        // when
        List<Order> results = orderService.findAll(orderNumber);

        // then
        assertThat(results, hasSize(1));
        assertThat(results.get(0).getOrderNumber(), is(orderNumber));
    }

    @Test
    void shouldFindOrderById() {
        // given
        long orderId = 1;
        Order order = new Order(new OrderNumber("numberOne"), null);
        order.setId(orderId);
        when(orderRepository.findById(orderId)).thenReturn(Optional.of(order));

        // when
        Order result = orderService.findById(orderId).orElse(null);

        // then
        assertThat(result, is(notNullValue()));
        assertThat(result.getId(), is(orderId));
        assertThat(result.getOrderNumber(), is(order.getOrderNumber()));
    }

    @Test
    void shouldSaveOrder() {
        // given
        Order order = new Order(new OrderNumber("numberOne"), null);
        when(orderRepository.save(order)).thenReturn(order);

        // when
        Order result = orderService.saveOrder(order);

        // then
        verify(orderRepository).save(order);
        assertThat(result, is(order));
    }

    @Test
    void shouldDeleteOrderById() {
        // given
        long orderId = 1;

        // when
        orderService.deleteById(orderId);

        // then
        verify(orderRepository).deleteById(orderId);
    }

}